package com.epam;

public class Fibonnachi {

    static final int F1 = 1;
    static final int F2 = 1;

    protected static int[] createFibonnachi(int number) {
        if (number == 0)
            return null;
        else if (number == 1)
            return new int[]{1};
        else {
            int[] result = new int[number];
            result[0] = F1;
            result[1] = F2;
            for (int i = 2; i < number; i++) {
                result[i] = (result[i - 2]) + (result[i - 1]);
            }
            return result;
        }
    }

}
