package com.epam;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Application {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int number;
        System.out.println("How much Fibonnachi number to calculate?");
        number = Integer.parseInt(reader.readLine());
        printArray(Fibonnachi.createFibonnachi(number));
        System.out.println("End...");
    }

    static protected void printArray(int[] arr) {
        if (arr == null) {
            return;
        }
        for (int num : arr) {
            System.out.print(num + " ");
        }
        System.out.println();
    }
}
